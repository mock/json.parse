describe('Testing patterns functionality', function () {
  it('should correct parse any type of structure', function () {

    expect(JSON.parse('{"prop":10,"prop2":2.05}')).toEqual({
      prop: 10,
      prop2: 2.05
    });

    expect(JSON.parse('{"prop":10,"prop2":2.05, "some Obj Pro": {"pr1": "blabla", "pr2": null, "boolT": true, "boolF": false}}')).toEqual({
      prop: 10,
      prop2: 2.05,
      'some Obj Pro': {
        pr1: "blabla",
        pr2: null,
        boolT: true,
        boolF: false
      }
    });

    expect(JSON.parse('[{"prop": 10, "prop2": 2.05}, 15, "some_sting_const"]')).toEqual([
      {
        prop: 10,
        prop2: 2.05
      },
      15,
      'some_sting_const'
    ]);
  });

  describe('Complex JSON.parse() testing', function () {
    jsonList.forEach(function (json, index) {
      it('final testing huge JSON. Case ' + index, function () {
        var res = JSON.parse(json[0]);

        expect(res).toBeDefined();

        expect(res).toEqual(json[1]);
      });
    });
  });
});