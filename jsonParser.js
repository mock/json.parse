JSON.originParse = JSON.parse;

/**
 * Custom JSON.parse method
 */
JSON.parse = (function() {

  var p = Parser.patterns;

  /**
   * Whitespaces
   */
  var wsp = p.rgx(/\s+/);

  /**
   * Optional whitespaces
   */
  var wspopt = p.opt(wsp);

  /**
   * Character
   */
  var char = p.rgx(/[^"]/i);

  /**
   * Text in quotes
   */
  var quoted = p.seq(p.txt('"'), p.rep(char), p.txt('"')).then(function (res) {
    return res[1].join('');
  });

  /**
   * Key
   */
  var key = p.seq(quoted, wspopt, p.txt(':'), wspopt).then(function (res) {
    return res[0];
  });

  /**
   * Number property
   */
  var num = p.rgx(/[\d\.\-]+/).then(function (r) {return parseFloat(r);});

  /**
   * Null property
   */
  var nll = p.txt('null').then(function () {return null;});

  /**
   * Null property
   */
  var bool = p.rgx(/false|true/i).then(function (r) {return r.length === 4;});

  /**
   * Node. Such complicated logic is intended for avoid circular dependency when
   * yet undefined Node links to itself.
   */
  var node = new Parser.Pattern(function (str, pos) {
    return p.any(num, nll, bool, quoted, arr, obj).exec(str, pos);
  });

  /**
   * Array property
   */
  var arr = p.seq(
    p.txt('['),
    p.rep(
      node,
      p.seq(p.txt(','), wspopt)
    ),
    p.txt(']')
  ).then(function (res) {
    return res[1].map(function (r) {
      return r;
    });
  });

  /**
   * Object property
   */
  var obj = p.seq(
    p.txt('{'),
    p.rep(
      p.seq(key, node),
      p.seq(p.txt(','), wspopt)
    ),
    p.txt('}')
  ).then(function (res) {
    return res[1].reduce(function (memo, r) {
      memo[r[0]] = r[1];
      return memo;
    }, {})
  });


  return function (str) {
    var result = p.any(arr, obj).exec(str, 0);
    return result && result.res;
  };
})();